# KCIDB Triage Tool

## Overview

The KCIDB Triage Tool is a comprehensive solution for managing kernel testing issues and incidents in KCIDB (Kernel CI Database). It provides two main interfaces:

1. A web-based user interface that allows users to:
   - Create and manage testing issues
   - Track and document incidents
   - View and update issue/incident details
   - Add comments and metadata

2. A command-line interface (CLI) tool that enables:
   - Programmatic creation of issues and incidents
   - Batch operations via REST API calls to the web server
   - Easy scripting and automation capabilities

## Running the Server and Web UI with Docker

```bash
GOOGLE_APPLICATION_CREDENTIALS=~/.config/gcloud/kernelci-production-ci-kernelci.json docker-compose up --build
```

The web interface will be available at http://localhost:5000.

## Server Configuration

`server/server_config.ini` contains the following configuration:

```ini
[database]
project_id = kernelci-production
topic_name = playground_kcidb_new
#topic_name = kernelci-production

[submission]
origin = kcidb-triage
```

* `project_id` specifies the Google Cloud project ID where KCIDB data is stored.
* `topic_name` specifies the KCIDB instance. Use `playground_kcidb_new` for staging and `kcidb_new` for production.
* `origin` sets the origin field to submit issues/incidents.

## Command-Line Tool

The kcidb-triage.py tool allows you to interact with the KCIDB Triage server from the command line.

### Configuration File

Create a file named `~/.kcidb-triage.conf` with the following content:

```ini
[user]
name = Your Name
email = your.email@example.com

[server]
url = http://localhost
port = 5000
```

### Creating an Issue

```bash
./kcidb-triage.py issues create --report_subject "Issue Subject" --culprit-type "code" --report_url "http://example.com" --comment "This is a comment" --misc '{"key": "value"}' --dry-run
```

Remove the `--dry-run` option to actually create the issue

### Creating Incidents

To create incidents associated with an existing issue:

```bash
cat file-with-ids-list.txt | ./kcidb-triage.py incidents create --issue_id "issue_id" --issue_version 1 --incident_type "build" --comment "This is a comment" --misc '{"key": "value"}' --dry-run
```

Remove the `--dry-run` option to actually create the incidents