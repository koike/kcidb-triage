#!/usr/bin/env python3

import argparse
import json
import requests
from configparser import ConfigParser
import os
import sys

def read_client_config():
    config = ConfigParser()
    config_path = os.path.expanduser("~/.kcidb-triage.conf")
    if not os.path.exists(config_path):
        raise FileNotFoundError(f"Configuration file not found: {config_path}")
    
    config.read(config_path)
    return config

def submit_report_to_server(server_url, endpoint, data):
    response = requests.post(f"{server_url}/{endpoint}", json=data)
    if response.status_code == 200:
        if data.get("dry_run"):
            print(json.dumps(response.json(), indent=4))
        else:
            print(f"Report submitted successfully. Submission ID: {response.json()['submission_id']}")
    else:
        print(f"Error: {response.json()['error']}")

def main():
    parser = argparse.ArgumentParser(description="KCIDB Triage Tool")
    subparsers = parser.add_subparsers(dest="command")

    issues_parser = subparsers.add_parser("issues")
    issues_subparsers = issues_parser.add_subparsers(dest="action")

    create_issue_parser = issues_subparsers.add_parser("create")
    create_issue_parser.add_argument("--report_subject", required=True, type=str)
    create_issue_parser.add_argument("--culprit-type", required=True, type=str, choices=["code", "tool", "harness"])
    create_issue_parser.add_argument("--report_url", type=str)
    create_issue_parser.add_argument("--comment", type=str)
    create_issue_parser.add_argument("--misc", type=str)
    create_issue_parser.add_argument("--dry-run", action="store_true", help="Print the report instead of submitting it")

    incidents_parser = subparsers.add_parser("incidents")
    incidents_subparsers = incidents_parser.add_subparsers(dest="action")

    create_incident_parser = incidents_subparsers.add_parser("create")
    create_incident_parser.add_argument("--issue_id", required=True, type=str)
    create_incident_parser.add_argument("--issue_version", required=True, type=int)
    create_incident_parser.add_argument("--incident_type", required=True, type=str, choices=["build", "test"])
    create_incident_parser.add_argument("--comment", type=str)
    create_incident_parser.add_argument("--misc", type=str)
    create_incident_parser.add_argument("--dry-run", action="store_true", help="Print the report instead of submitting it")

    args = parser.parse_args()
    config = read_client_config()

    server_url = f"{config.get('server', 'url')}:{config.get('server', 'port')}"
    user_name = config.get("user", "name")
    user_email = config.get("user", "email")

    if args.command == "issues" and args.action == "create":
        data = {
            "name": user_name,
            "email": user_email,
            "report_subject": args.report_subject,
            "culprit_type": args.culprit_type,
            "report_url": args.report_url,
            "comment": args.comment,
            "misc": args.misc,
            "dry_run": args.dry_run
        }
        submit_report_to_server(server_url, "submit_issue", data)

    elif args.command == "incidents" and args.action == "create":
        ids_list = sys.stdin.read().strip()
        data = {
            "name": user_name,
            "email": user_email,
            "issue_id": args.issue_id,
            "issue_version": args.issue_version,
            "incident_type": args.incident_type,
            "ids_list": ids_list,
            "comment": args.comment,
            "misc": args.misc,
            "dry_run": args.dry_run
        }
        submit_report_to_server(server_url, "submit_incidents", data)

if __name__ == "__main__":
    main()
