# syntax=docker/dockerfile:1

FROM python:3.11-slim

WORKDIR /app

COPY server/* /app/

COPY server/templates/index.html /app/templates/

COPY requirements.txt /app/

RUN apt-get update && apt-get install -y git libpq-dev build-essential python3-dev

RUN pip install --no-cache-dir -r requirements.txt

ENV GOOGLE_APPLICATION_CREDENTIALS=/app/credentials/kernelci-production-ci-kernelci.json

VOLUME ["/app/credentials"]

EXPOSE 5000

CMD ["python3", "kcidb-triage-server.py"]